/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20210331 (64-bit version)
 * Copyright (c) 2000 - 2021 Intel Corporation
 * 
 * Disassembly of fidt.dat, Sun Jul 11 06:28:47 2021
 *
 * ACPI Data Table [FIDT]
 *
 * Format: [HexOffset DecimalOffset ByteLength]  FieldName : FieldValue
 */

[000h 0000   4]                    Signature : "FIDT"    
[004h 0004   4]                 Table Length : 0000009C
[008h 0008   1]                     Revision : 01
[009h 0009   1]                     Checksum : 20
[00Ah 0010   6]                       Oem ID : "_ASUS_"
[010h 0016   8]                 Oem Table ID : "Notebook"
[018h 0024   4]                 Oem Revision : 01072009
[01Ch 0028   4]              Asl Compiler ID : "AMI "
[020h 0032   4]        Asl Compiler Revision : 00010013


**** Unknown ACPI table signature [FIDT]


Raw Table Data: Length 156 (0x9C)

    0000: 46 49 44 54 9C 00 00 00 01 20 5F 41 53 55 53 5F  // FIDT..... _ASUS_
    0010: 4E 6F 74 65 62 6F 6F 6B 09 20 07 01 41 4D 49 20  // Notebook. ..AMI 
    0020: 13 00 01 00 24 46 49 44 04 78 00 31 41 58 47 4B  // ....$FID.x.1AXGK
    0030: 33 30 36 00 49 73 44 A1 45 E6 4A FC 61 89 22 41  // 306.IsD.E.J.a."A
    0040: 7F 05 DD 5B 30 35 00 31 39 00 30 33 00 30 36 00  // ...[05.19.03.06.
    0050: E5 07 05 07 14 1C 29 FF FF 5F 41 53 55 53 5F 4E  // ......).._ASUS_N
    0060: 6F 74 65 62 6F 6F 6B 31 00 00 00 FF FF FF FF FF  // otebook1........
    0070: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0080: FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF  // ................
    0090: FF FF FF FF FF FF FF FF FF FF FF FF              // ............
