/*
 * Intel ACPI Component Architecture
 * AML/ASL+ Disassembler version 20210331 (64-bit version)
 * Copyright (c) 2000 - 2021 Intel Corporation
 * 
 * Disassembling to symbolic ASL+ operators
 *
 * Disassembly of ssdt14.dat, Sun Jul 11 06:28:47 2021
 *
 * Original Table Header:
 *     Signature        "SSDT"
 *     Length           0x0000112E (4398)
 *     Revision         0x01
 *     Checksum         0xEA
 *     OEM ID           "AMD"
 *     OEM Table ID     "AmdTable"
 *     OEM Revision     0x00000001 (1)
 *     Compiler ID      "INTL"
 *     Compiler Version 0x20190509 (538510601)
 */
DefinitionBlock ("", "SSDT", 1, "AMD", "AmdTable", 0x00000001)
{
    External (_SB_.PCI0.SBRG.CCI0, IntObj)
    External (_SB_.PCI0.SBRG.CCI1, IntObj)
    External (_SB_.PCI0.SBRG.CCI2, IntObj)
    External (_SB_.PCI0.SBRG.CCI3, IntObj)
    External (_SB_.PCI0.SBRG.CTL0, IntObj)
    External (_SB_.PCI0.SBRG.CTL1, IntObj)
    External (_SB_.PCI0.SBRG.CTL2, IntObj)
    External (_SB_.PCI0.SBRG.CTL3, IntObj)
    External (_SB_.PCI0.SBRG.CTL4, IntObj)
    External (_SB_.PCI0.SBRG.CTL5, IntObj)
    External (_SB_.PCI0.SBRG.CTL6, IntObj)
    External (_SB_.PCI0.SBRG.CTL7, IntObj)
    External (_SB_.PCI0.SBRG.MGI0, IntObj)
    External (_SB_.PCI0.SBRG.MGI1, IntObj)
    External (_SB_.PCI0.SBRG.MGI2, IntObj)
    External (_SB_.PCI0.SBRG.MGI3, IntObj)
    External (_SB_.PCI0.SBRG.MGI4, IntObj)
    External (_SB_.PCI0.SBRG.MGI5, IntObj)
    External (_SB_.PCI0.SBRG.MGI6, IntObj)
    External (_SB_.PCI0.SBRG.MGI7, IntObj)
    External (_SB_.PCI0.SBRG.MGI8, IntObj)
    External (_SB_.PCI0.SBRG.MGI9, IntObj)
    External (_SB_.PCI0.SBRG.MGIA, IntObj)
    External (_SB_.PCI0.SBRG.MGIB, IntObj)
    External (_SB_.PCI0.SBRG.MGIC, IntObj)
    External (_SB_.PCI0.SBRG.MGID, IntObj)
    External (_SB_.PCI0.SBRG.MGIE, IntObj)
    External (_SB_.PCI0.SBRG.MGIF, IntObj)
    External (_SB_.PCI0.SBRG.MGO0, IntObj)
    External (_SB_.PCI0.SBRG.MGO1, IntObj)
    External (_SB_.PCI0.SBRG.MGO2, IntObj)
    External (_SB_.PCI0.SBRG.MGO3, IntObj)
    External (_SB_.PCI0.SBRG.MGO4, IntObj)
    External (_SB_.PCI0.SBRG.MGO5, IntObj)
    External (_SB_.PCI0.SBRG.MGO6, IntObj)
    External (_SB_.PCI0.SBRG.MGO7, IntObj)
    External (_SB_.PCI0.SBRG.MGO8, IntObj)
    External (_SB_.PCI0.SBRG.MGO9, IntObj)
    External (_SB_.PCI0.SBRG.MGOA, IntObj)
    External (_SB_.PCI0.SBRG.MGOB, IntObj)
    External (_SB_.PCI0.SBRG.MGOC, IntObj)
    External (_SB_.PCI0.SBRG.MGOD, IntObj)
    External (_SB_.PCI0.SBRG.MGOE, IntObj)
    External (_SB_.PCI0.SBRG.MGOF, IntObj)
    External (_SB_.PCI0.SBRG.SEC1, MethodObj)    // 1 Arguments
    External (M000, MethodObj)    // 1 Arguments
    External (M013, MethodObj)    // 4 Arguments
    External (M037, DeviceObj)
    External (M046, DeviceObj)
    External (M047, DeviceObj)
    External (M049, MethodObj)    // 2 Arguments
    External (M050, DeviceObj)
    External (M051, DeviceObj)
    External (M052, DeviceObj)
    External (M053, DeviceObj)
    External (M054, DeviceObj)
    External (M055, DeviceObj)
    External (M056, DeviceObj)
    External (M057, DeviceObj)
    External (M058, DeviceObj)
    External (M059, DeviceObj)
    External (M062, DeviceObj)
    External (M068, DeviceObj)
    External (M069, DeviceObj)
    External (M070, DeviceObj)
    External (M071, DeviceObj)
    External (M072, DeviceObj)
    External (M074, DeviceObj)
    External (M075, DeviceObj)
    External (M076, DeviceObj)
    External (M077, DeviceObj)
    External (M078, DeviceObj)
    External (M079, DeviceObj)
    External (M080, DeviceObj)
    External (M081, DeviceObj)
    External (M082, FieldUnitObj)
    External (M083, FieldUnitObj)
    External (M084, FieldUnitObj)
    External (M085, FieldUnitObj)
    External (M086, FieldUnitObj)
    External (M087, FieldUnitObj)
    External (M088, FieldUnitObj)
    External (M089, FieldUnitObj)
    External (M090, FieldUnitObj)
    External (M091, FieldUnitObj)
    External (M092, FieldUnitObj)
    External (M093, FieldUnitObj)
    External (M094, FieldUnitObj)
    External (M095, FieldUnitObj)
    External (M096, FieldUnitObj)
    External (M097, FieldUnitObj)
    External (M098, FieldUnitObj)
    External (M099, FieldUnitObj)
    External (M100, FieldUnitObj)
    External (M101, FieldUnitObj)
    External (M102, FieldUnitObj)
    External (M103, FieldUnitObj)
    External (M104, FieldUnitObj)
    External (M105, FieldUnitObj)
    External (M106, FieldUnitObj)
    External (M107, FieldUnitObj)
    External (M108, FieldUnitObj)
    External (M109, FieldUnitObj)
    External (M110, FieldUnitObj)
    External (M115, BuffObj)
    External (M116, BuffFieldObj)
    External (M117, BuffFieldObj)
    External (M118, BuffFieldObj)
    External (M119, BuffFieldObj)
    External (M120, BuffFieldObj)
    External (M122, FieldUnitObj)
    External (M127, DeviceObj)
    External (M128, FieldUnitObj)
    External (M131, FieldUnitObj)
    External (M132, FieldUnitObj)
    External (M133, FieldUnitObj)
    External (M134, FieldUnitObj)
    External (M135, FieldUnitObj)
    External (M136, FieldUnitObj)
    External (M220, FieldUnitObj)
    External (M221, FieldUnitObj)
    External (M226, FieldUnitObj)
    External (M227, DeviceObj)
    External (M229, FieldUnitObj)
    External (M231, FieldUnitObj)
    External (M233, FieldUnitObj)
    External (M235, FieldUnitObj)
    External (M251, FieldUnitObj)
    External (M280, FieldUnitObj)
    External (M290, FieldUnitObj)
    External (M310, FieldUnitObj)
    External (M320, FieldUnitObj)
    External (M321, FieldUnitObj)
    External (M322, FieldUnitObj)
    External (M323, FieldUnitObj)
    External (M324, FieldUnitObj)
    External (M325, FieldUnitObj)
    External (M326, FieldUnitObj)
    External (M327, FieldUnitObj)
    External (M328, FieldUnitObj)
    External (M329, DeviceObj)
    External (M32A, DeviceObj)
    External (M32B, DeviceObj)
    External (M330, DeviceObj)
    External (M331, FieldUnitObj)
    External (M378, FieldUnitObj)
    External (M379, FieldUnitObj)
    External (M380, FieldUnitObj)
    External (M381, FieldUnitObj)
    External (M382, FieldUnitObj)
    External (M383, FieldUnitObj)
    External (M384, FieldUnitObj)
    External (M385, FieldUnitObj)
    External (M386, FieldUnitObj)
    External (M387, FieldUnitObj)
    External (M388, FieldUnitObj)
    External (M389, FieldUnitObj)
    External (M390, FieldUnitObj)
    External (M391, FieldUnitObj)
    External (M392, FieldUnitObj)
    External (M404, DeviceObj)
    External (M414, FieldUnitObj)
    External (M444, FieldUnitObj)
    External (M449, FieldUnitObj)

    Scope (\_SB)
    {
        OperationRegion (PM0A, SystemMemory, M322, 0x02)
        Field (PM0A, ByteAcc, Lock, Preserve)
        {
            VER0,   8, 
            VER1,   8
        }

        OperationRegion (PM04, SystemMemory, M323, 0x04)
        Field (PM04, ByteAcc, Lock, Preserve)
        {
            CCI0,   8, 
            CCI1,   8, 
            CCI2,   8, 
            CCI3,   8
        }

        OperationRegion (PM05, SystemMemory, M324, 0x08)
        Field (PM05, ByteAcc, Lock, Preserve)
        {
            CTL0,   8, 
            CTL1,   8, 
            CTL2,   8, 
            CTL3,   8, 
            CTL4,   8, 
            CTL5,   8, 
            CTL6,   8, 
            CTL7,   8
        }

        OperationRegion (PM06, SystemMemory, M325, 0x10)
        Field (PM06, ByteAcc, Lock, Preserve)
        {
            MGI0,   8, 
            MGI1,   8, 
            MGI2,   8, 
            MGI3,   8, 
            MGI4,   8, 
            MGI5,   8, 
            MGI6,   8, 
            MGI7,   8, 
            MGI8,   8, 
            MGI9,   8, 
            MGIA,   8, 
            MGIB,   8, 
            MGIC,   8, 
            MGID,   8, 
            MGIE,   8, 
            MGIF,   8
        }

        OperationRegion (PM07, SystemMemory, M326, 0x10)
        Field (PM07, ByteAcc, Lock, Preserve)
        {
            MGO0,   8, 
            MGO1,   8, 
            MGO2,   8, 
            MGO3,   8, 
            MGO4,   8, 
            MGO5,   8, 
            MGO6,   8, 
            MGO7,   8, 
            MGO8,   8, 
            MGO9,   8, 
            MGOA,   8, 
            MGOB,   8, 
            MGOC,   8, 
            MGOD,   8, 
            MGOE,   8, 
            MGOF,   8
        }

        Device (UBTC)
        {
            Name (_HID, EisaId ("USBC000"))  // _HID: Hardware ID
            Name (_CID, EisaId ("PNP0CA0"))  // _CID: Compatible ID
            Name (_UID, Zero)  // _UID: Unique ID
            Name (_DDN, "USB Type C")  // _DDN: DOS Device Name
            Name (_ADR, Zero)  // _ADR: Address
            Name (M311, Buffer (0x14)
            {
                /* 0000 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                /* 0008 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // ........
                /* 0010 */  0x00, 0x00, 0x00, 0x00                           // ....
            })
            Name (CRS, ResourceTemplate ()
            {
                Memory32Fixed (ReadWrite,
                    0x00000000,         // Address Base
                    0x00001000,         // Address Length
                    _Y25)
            })
            Device (CR01)
            {
                Name (_ADR, Zero)  // _ADR: Address
                Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
                {
                    CreateDWordField (M311, Zero, M312)
                    CreateDWordField (M311, 0x04, M313)
                    CreateDWordField (M311, 0x08, M314)
                    CreateDWordField (M311, 0x0C, M315)
                    CreateDWordField (M311, 0x10, M316)
                    Local0 = M310 /* External reference */
                    If (Local0)
                    {
                        Local0 += 0x4D
                        M312 = M013 ((Local0 + Zero), Zero, Zero, 0x20)
                        M313 = M013 ((Local0 + 0x04), Zero, Zero, 0x20)
                        M314 = M013 ((Local0 + 0x08), Zero, Zero, 0x20)
                        M315 = M013 ((Local0 + 0x0C), Zero, Zero, 0x20)
                        M316 = M013 ((Local0 + 0x10), Zero, Zero, 0x20)
                    }

                    Return (M311) /* \_SB_.UBTC.M311 */
                }
            }

            Device (CR02)
            {
                Name (_ADR, One)  // _ADR: Address
                Method (_PLD, 0, NotSerialized)  // _PLD: Physical Location of Device
                {
                    CreateDWordField (M311, Zero, M312)
                    CreateDWordField (M311, 0x04, M313)
                    CreateDWordField (M311, 0x08, M314)
                    CreateDWordField (M311, 0x0C, M315)
                    CreateDWordField (M311, 0x10, M316)
                    Local0 = M310 /* External reference */
                    If (Local0)
                    {
                        Local0 += 0x61
                        M312 = M013 ((Local0 + Zero), Zero, Zero, 0x20)
                        M313 = M013 ((Local0 + 0x04), Zero, Zero, 0x20)
                        M314 = M013 ((Local0 + 0x08), Zero, Zero, 0x20)
                        M315 = M013 ((Local0 + 0x0C), Zero, Zero, 0x20)
                        M316 = M013 ((Local0 + 0x10), Zero, Zero, 0x20)
                    }

                    Return (M311) /* \_SB_.UBTC.M311 */
                }
            }

            Method (_CRS, 0, Serialized)  // _CRS: Current Resource Settings
            {
                CreateDWordField (CRS, \_SB.UBTC._Y25._BAS, M317)  // _BAS: Base Address
                Local0 = M310 /* External reference */
                M317 = (Local0 + 0x1D)
                Return (CRS) /* \_SB_.UBTC.CRS_ */
            }

            Method (_STA, 0, NotSerialized)  // _STA: Status
            {
                If ((M049 (M128, 0x78) == One))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }

            OperationRegion (PM08, SystemMemory, M320, 0x30)
            Field (PM08, ByteAcc, Lock, Preserve)
            {
                VER0,   8, 
                VER1,   8, 
                RSV0,   8, 
                RSV1,   8, 
                CCI0,   8, 
                CCI1,   8, 
                CCI2,   8, 
                CCI3,   8, 
                CTL0,   8, 
                CTL1,   8, 
                CTL2,   8, 
                CTL3,   8, 
                CTL4,   8, 
                CTL5,   8, 
                CTL6,   8, 
                CTL7,   8, 
                MGI0,   8, 
                MGI1,   8, 
                MGI2,   8, 
                MGI3,   8, 
                MGI4,   8, 
                MGI5,   8, 
                MGI6,   8, 
                MGI7,   8, 
                MGI8,   8, 
                MGI9,   8, 
                MGIA,   8, 
                MGIB,   8, 
                MGIC,   8, 
                MGID,   8, 
                MGIE,   8, 
                MGIF,   8, 
                MGO0,   8, 
                MGO1,   8, 
                MGO2,   8, 
                MGO3,   8, 
                MGO4,   8, 
                MGO5,   8, 
                MGO6,   8, 
                MGO7,   8, 
                MGO8,   8, 
                MGO9,   8, 
                MGOA,   8, 
                MGOB,   8, 
                MGOC,   8, 
                MGOD,   8, 
                MGOE,   8, 
                MGOF,   8
            }

            Method (M318, 0, Serialized)
            {
                MGI0 = \_SB.PCI0.SBRG.MGI0 /* External reference */
                MGI1 = \_SB.PCI0.SBRG.MGI1 /* External reference */
                MGI2 = \_SB.PCI0.SBRG.MGI2 /* External reference */
                MGI3 = \_SB.PCI0.SBRG.MGI3 /* External reference */
                MGI4 = \_SB.PCI0.SBRG.MGI4 /* External reference */
                MGI5 = \_SB.PCI0.SBRG.MGI5 /* External reference */
                MGI6 = \_SB.PCI0.SBRG.MGI6 /* External reference */
                MGI7 = \_SB.PCI0.SBRG.MGI7 /* External reference */
                MGI8 = \_SB.PCI0.SBRG.MGI8 /* External reference */
                MGI9 = \_SB.PCI0.SBRG.MGI9 /* External reference */
                MGIA = \_SB.PCI0.SBRG.MGIA /* External reference */
                MGIB = \_SB.PCI0.SBRG.MGIB /* External reference */
                MGIC = \_SB.PCI0.SBRG.MGIC /* External reference */
                MGID = \_SB.PCI0.SBRG.MGID /* External reference */
                MGIE = \_SB.PCI0.SBRG.MGIE /* External reference */
                MGIF = \_SB.PCI0.SBRG.MGIF /* External reference */
                CCI0 = \_SB.PCI0.SBRG.CCI0 /* External reference */
                CCI1 = \_SB.PCI0.SBRG.CCI1 /* External reference */
                CCI2 = \_SB.PCI0.SBRG.CCI2 /* External reference */
                CCI3 = \_SB.PCI0.SBRG.CCI3 /* External reference */
            }

            Method (_DSM, 4, Serialized)  // _DSM: Device-Specific Method
            {
                If ((Arg0 == ToUUID ("6f8398c2-7ca4-11e4-ad36-631042b5008f") /* Unknown UUID */))
                {
                    If ((ToInteger (Arg2) == Zero))
                    {
                        Return (Buffer (One)
                        {
                             0x0F                                             // .
                        })
                    }
                    ElseIf ((ToInteger (Arg2) == One))
                    {
                        M000 (0xA8)
                        \_SB.PCI0.SBRG.MGO0 = MGO0 /* \_SB_.UBTC.MGO0 */
                        \_SB.PCI0.SBRG.MGO1 = MGO1 /* \_SB_.UBTC.MGO1 */
                        \_SB.PCI0.SBRG.MGO2 = MGO2 /* \_SB_.UBTC.MGO2 */
                        \_SB.PCI0.SBRG.MGO3 = MGO3 /* \_SB_.UBTC.MGO3 */
                        \_SB.PCI0.SBRG.MGO4 = MGO4 /* \_SB_.UBTC.MGO4 */
                        \_SB.PCI0.SBRG.MGO5 = MGO5 /* \_SB_.UBTC.MGO5 */
                        \_SB.PCI0.SBRG.MGO6 = MGO6 /* \_SB_.UBTC.MGO6 */
                        \_SB.PCI0.SBRG.MGO7 = MGO7 /* \_SB_.UBTC.MGO7 */
                        \_SB.PCI0.SBRG.MGO8 = MGO8 /* \_SB_.UBTC.MGO8 */
                        \_SB.PCI0.SBRG.MGO9 = MGO9 /* \_SB_.UBTC.MGO9 */
                        \_SB.PCI0.SBRG.MGOA = MGOA /* \_SB_.UBTC.MGOA */
                        \_SB.PCI0.SBRG.MGOB = MGOB /* \_SB_.UBTC.MGOB */
                        \_SB.PCI0.SBRG.MGOC = MGOC /* \_SB_.UBTC.MGOC */
                        \_SB.PCI0.SBRG.MGOD = MGOD /* \_SB_.UBTC.MGOD */
                        \_SB.PCI0.SBRG.MGOE = MGOE /* \_SB_.UBTC.MGOE */
                        \_SB.PCI0.SBRG.MGOF = MGOF /* \_SB_.UBTC.MGOF */
                        \_SB.PCI0.SBRG.CTL0 = CTL0 /* \_SB_.UBTC.CTL0 */
                        \_SB.PCI0.SBRG.CTL1 = CTL1 /* \_SB_.UBTC.CTL1 */
                        \_SB.PCI0.SBRG.CTL2 = CTL2 /* \_SB_.UBTC.CTL2 */
                        \_SB.PCI0.SBRG.CTL3 = CTL3 /* \_SB_.UBTC.CTL3 */
                        \_SB.PCI0.SBRG.CTL4 = CTL4 /* \_SB_.UBTC.CTL4 */
                        \_SB.PCI0.SBRG.CTL5 = CTL5 /* \_SB_.UBTC.CTL5 */
                        \_SB.PCI0.SBRG.CTL6 = CTL6 /* \_SB_.UBTC.CTL6 */
                        \_SB.PCI0.SBRG.CTL7 = CTL7 /* \_SB_.UBTC.CTL7 */
                        \_SB.PCI0.SBRG.CCI0 = Zero
                        \_SB.PCI0.SBRG.CCI1 = Zero
                        \_SB.PCI0.SBRG.CCI2 = Zero
                        \_SB.PCI0.SBRG.CCI3 = Zero
                        \_SB.PCI0.SBRG.SEC1 (0x18)
                        M000 (0xA9)
                    }
                    ElseIf ((ToInteger (Arg2) == 0x02))
                    {
                        M000 (0xAA)
                        M318 ()
                        M000 (0xAB)
                    }
                }

                Return (Zero)
            }
        }
    }
}

